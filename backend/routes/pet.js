const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");
const db = require("../db");

router.get("/", (request, response) => {
  const connection = db.openConnection();
  const statement = `select * from Pet`;
  connection.query(statement, (error, result) => {
    connection.end();
      response.send(utils.createResult(error, result));
  });
});

router.post("/add", (request, response) => {
  const { name,price, age } = request.body;
  const connection = db.openConnection();
  const statement = `
    insert into Pet ( name,price,age) values( '${name}',${price},${age})`;
    connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:name", (request, response) => {
  const { name } = request.params;
  const { price } = request.body;
  const statement = `
    update Pet set price=${price} where name = '${name}'`;
  const connection = db.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.delete("/remove/:name", (request, response) => {
  const { name } = request.params;
  const statement = `
    delete from Pet where name = '${name}'`;
  const connection = db.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
